# Mockup loader CI routines for Gitlab

See docs and examples at https://gitlab.com/atsybulsky/ut-monitor-example

In version 2 (after tag `v1`):
- create monitor branch, and it's gitignore and gitattributes automatically if the branch is missing

### Dev notes

- https://gitlab.com/gitlab-org/gitlab/-/issues/8177
